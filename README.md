In this project, I used a 9 layered neural network as specified by NVIDIA's paper to simulate a self-driving car.
The blog post by NVIDIA - "End-to-End Deep Learning for Self-Driving Cars" outlines how data was collected and how it was used to train a model for the purpose of autonomous driving.
I simulated this by driving a car using Udacity's self driving simulator and then using the data generated to train a CNN. The network consisted of the following layers:
* 1 normalization layer 
* 5 convolutional layers 
* 3 fully connected layers